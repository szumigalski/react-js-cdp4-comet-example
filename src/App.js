import React, { useState, useEffect } from 'react'
import LoginComponent from './components/LoginComponent';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { initSession, getIteration } from './cdp4/getCdp4Elements'
import SiteDirectoryComponent from './components/SiteDirectoryComponent'

function App() {
  const [loading, setLoading] = useState(null)
  const [session, setSession] = useState(null)
  const [, setDataTree] = useState([])

  useEffect(() => {
    switch(loading) {
      case 'Init session...':
        initSession(setSession, setLoading, setDataTree)
        break
      case 'Init iteration...':
        getIteration(session, setLoading)
        break
      default:
        break
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading])

  return (
    <div style={{width: '100%', height: '100vh'}}>
      <ToastContainer />
      {loading === 'ready'
        ? <SiteDirectoryComponent session={session} />
        : <LoginComponent
            setLoading={setLoading}
            loading={loading}
            />}
    </div>
  );
}

export default App;
