const ClassKind = {
    /**
     * Assertian that the Class is an instance of ActionItem
     */
    ActionItem: 0,
    /**
     * Assertian that the Class is an instance of ActualFiniteState
     */
    ActualFiniteState: 1,
    /**
     * Assertian that the Class is an instance of ActualFiniteStateList
     */
    ActualFiniteStateList: 2,
    /**
     * Assertian that the Class is an instance of Alias
     */
    Alias: 3,
    /**
     * Assertian that the Class is an instance of AndExpression
     */
    AndExpression: 4,
    /**
     * Assertian that the Class is an instance of Approval
     */
    Approval: 5,
    /**
     * Assertian that the Class is an instance of ArrayParameterType
     */
    ArrayParameterType: 6,
    /**
     * Assertian that the Class is an instance of BinaryNote
     */
    BinaryNote: 7,
    /**
     * Assertian that the Class is an instance of BinaryRelationship
     */
    BinaryRelationship: 8,
    /**
     * Assertian that the Class is an instance of BinaryRelationshipRule
     */
    BinaryRelationshipRule: 9,
    /**
     * Assertian that the Class is an instance of book
     */
    Book: 10,
    /**
     * Assertian that the Class is an instance of BooleanExpression
     */
    BooleanExpression: 11,
    /**
     * Assertian that the Class is an instance of BooleanParameterType
     */
    BooleanParameterType: 12,
    /**
     * Assertian that the Class is an instance of Bounds
     */
    Bounds: 13,
    /**
     * Assertian that the Class is an instance of BuiltInRuleVerification
     */
    BuiltInRuleVerification: 14,
    /**
     * Assertian that the Class is an instance of Category
     */
    Category: 15,
    /**
     * Assertian that the Class is an instance of ChangeProposal
     */
    ChangeProposal: 16,
    /**
     * Assertian that the Class is an instance of ChangeRequest
     */
    ChangeRequest: 17,
    /**
     * Assertian that the Class is an instance of Citation
     */
    Citation: 18,
    /**
     * Assertian that the Class is an instance of Color
     */
    Color: 19,
    /**
     * Assertian that the Class is an instance of CommonFileStore
     */
    CommonFileStore: 20,
    /**
     * Assertian that the Class is an instance of CompoundParameterType
     */
    CompoundParameterType: 21,
    /**
     * Assertian that the Class is an instance of Constant
     */
    Constant: 22,
    /**
     * Assertian that the Class is an instance of ContractChangeNotice
     */
    ContractChangeNotice: 23,
    /**
     * Assertian that the Class is an instance of ContractDeviation
     */
    ContractDeviation: 24,
    /**
     * Assertian that the Class is an instance of ConversionBasedUnit
     */
    ConversionBasedUnit: 25,
    /**
     * Assertian that the Class is an instance of CyclicRatioScale
     */
    CyclicRatioScale: 26,
    /**
     * Assertian that the Class is an instance of DateParameterType
     */
    DateParameterType: 27,
    /**
     * Assertian that the Class is an instance of DateTimeParameterType
     */
    DateTimeParameterType: 28,
    /**
     * Assertian that the Class is an instance of DecompositionRule
     */
    DecompositionRule: 29,
    /**
     * Assertian that the Class is an instance of DefinedThing
     */
    DefinedThing: 30,
    /**
     * Assertian that the Class is an instance of Definition
     */
    Definition: 31,
    /**
     * Assertian that the Class is an instance of DerivedQuantityKind
     */
    DerivedQuantityKind: 32,
    /**
     * Assertian that the Class is an instance of DerivedUnit
     */
    DerivedUnit: 33,
    /**
     * Assertian that the Class is an instance of DiagramCanvas
     */
    DiagramCanvas: 34,
    /**
     * Assertian that the Class is an instance of DiagramEdge
     */
    DiagramEdge: 35,
    /**
     * Assertian that the Class is an instance of DiagramElementContainer
     */
    DiagramElementContainer: 36,
    /**
     * Assertian that the Class is an instance of DiagramElementThing
     */
    DiagramElementThing: 37,
    /**
     * Assertian that the Class is an instance of DiagrammingStyle
     */
    DiagrammingStyle: 38,
    /**
     * Assertian that the Class is an instance of DiagramObject
     */
    DiagramObject: 39,
    /**
     * Assertian that the Class is an instance of DiagramShape
     */
    DiagramShape: 40,
    /**
     * Assertian that the Class is an instance of DiagramThingBase
     */
    DiagramThingBase: 41,
    /**
     * Assertian that the Class is an instance of DiscussionItem
     */
    DiscussionItem: 42,
    /**
     * Assertian that the Class is an instance of DomainFileStore
     */
    DomainFileStore: 43,
    /**
     * Assertian that the Class is an instance of DomainOfExpertise
     */
    DomainOfExpertise: 44,
    /**
     * Assertian that the Class is an instance of DomainOfExpertiseGroup
     */
    DomainOfExpertiseGroup: 45,
    /**
     * Assertian that the Class is an instance of ElementBase
     */
    ElementBase: 46,
    /**
     * Assertian that the Class is an instance of ElementDefinition
     */
    ElementDefinition: 47,
    /**
     * Assertian that the Class is an instance of ElementUsage
     */
    ElementUsage: 48,
    /**
     * Assertian that the Class is an instance of EmailAddress
     */
    EmailAddress: 49,
    /**
     * Assertian that the Class is an instance of EngineeringModel
     */
    EngineeringModel: 50,
    /**
     * Assertian that the Class is an instance of EngineeringModelDataAnnotation
     */
    EngineeringModelDataAnnotation: 51,
    /**
     * Assertian that the Class is an instance of EngineeringModelDataDiscussionItem
     */
    EngineeringModelDataDiscussionItem: 52,
    /**
     * Assertian that the Class is an instance of EngineeringModelDataNote
     */
    EngineeringModelDataNote: 53,
    /**
     * Assertian that the Class is an instance of EngineeringModelSetup
     */
    EngineeringModelSetup: 54,
    /**
     * Assertian that the Class is an instance of EnumerationParameterType
     */
    EnumerationParameterType: 55,
    /**
     * Assertian that the Class is an instance of EnumerationValueDefinition
     */
    EnumerationValueDefinition: 56,
    /**
     * Assertian that the Class is an instance of ExclusiveOrExpression
     */
    ExclusiveOrExpression: 57,
    /**
     * Assertian that the Class is an instance of ExternalIdentifierMap
     */
    ExternalIdentifierMap: 58,
    /**
     * Assertian that the Class is an instance of File
     */
    File: 59,
    /**
     * Assertian that the Class is an instance of FileRevision
     */
    FileRevision: 60,
    /**
     * Assertian that the Class is an instance of FileStore
     */
    FileStore: 61,
    /**
     * Assertian that the Class is an instance of FileType
     */
    FileType: 62,
    /**
     * Assertian that the Class is an instance of Folder
     */
    Folder: 63,
    /**
     * Assertian that the Class is an instance of GenericAnnotation
     */
    GenericAnnotation: 64,
    /**
     * Assertian that the Class is an instance of Glossary
     */
    Glossary: 65,
    /**
     * Assertian that the Class is an instance of Goal
     */
    Goal: 66,
    /**
     * Assertian that the Class is an instance of HyperLink
     */
    HyperLink: 67,
    /**
     * Assertian that the Class is an instance of IdCorrespondence
     */
    IdCorrespondence: 68,
    /**
     * Assertian that the Class is an instance of IntervalScale
     */
    IntervalScale: 69,
    /**
     * Assertian that the Class is an instance of Iteration
     */
    Iteration: 70,
    /**
     * Assertian that the Class is an instance of IterationSetup
     */
    IterationSetup: 71,
    /**
     * Assertian that the Class is an instance of LinearConversionUnit
     */
    LinearConversionUnit: 72,
    /**
     * Assertian that the Class is an instance of LogarithmicScale
     */
    LogarithmicScale: 73,
    /**
     * Assertian that the Class is an instance of MappingToReferenceScale
     */
    MappingToReferenceScale: 74,
    /**
     * Assertian that the Class is an instance of MeasurementScale
     */
    MeasurementScale: 75,
    /**
     * Assertian that the Class is an instance of MeasurementUnit
     */
    MeasurementUnit: 76,
    /**
     * Assertian that the Class is an instance of ModellingAnnotationItem
     */
    ModellingAnnotationItem: 77,
    /**
     * Assertian that the Class is an instance of ModellingThingReference
     */
    ModellingThingReference: 78,
    /**
     * Assertian that the Class is an instance of ModelLogEntry
     */
    ModelLogEntry: 79,
    /**
     * Assertian that the Class is an instance of ModelReferenceDataLibrary
     */
    ModelReferenceDataLibrary: 80,
    /**
     * Assertian that the Class is an instance of MultiRelationship
     */
    MultiRelationship: 81,
    /**
     * Assertian that the Class is an instance of MultiRelationshipRule
     */
    MultiRelationshipRule: 82,
    /**
     * Assertian that the Class is an instance of NaturalLanguage
     */
    NaturalLanguage: 83,
    /**
     * Assertian that the Class is an instance of NestedElement
     */
    NestedElement: 84,
    /**
     * Assertian that the Class is an instance of NestedParameter
     */
    NestedParameter: 85,
    /**
     * Assertian that the Class is an instance of note
     */
    Note: 86,
    /**
     * Assertian that the Class is an instance of NotExpression
     */
    NotExpression: 87,
    /**
     * Assertian that the Class is an instance of NotThing
     */
    NotThing: 88,
    /**
     * Assertian that the Class is an instance of option
     */
    Option: 89,
    /**
     * Assertian that the Class is an instance of OrdinalScale
     */
    OrdinalScale: 90,
    /**
     * Assertian that the Class is an instance of OrExpression
     */
    OrExpression: 91,
    /**
     * Assertian that the Class is an instance of Organization
     */
    Organization: 92,
    /**
     * Assertian that the Class is an instance of OwnedStyle
     */
    OwnedStyle: 93,
    /**
     * Assertian that the Class is an instance of page
     */
    Page: 94,
    /**
     * Assertian that the Class is an instance of Parameter
     */
    Parameter: 95,
    /**
     * Assertian that the Class is an instance of ParameterBase
     */
    ParameterBase: 96,
    /**
     * Assertian that the Class is an instance of ParameterGroup
     */
    ParameterGroup: 97,
    /**
     * Assertian that the Class is an instance of ParameterizedCategoryRule
     */
    ParameterizedCategoryRule: 98,
    /**
     * Assertian that the Class is an instance of ParameterOrOverrideBase
     */
    ParameterOrOverrideBase: 99,
    /**
     * Assertian that the Class is an instance of ParameterOverride
     */
    ParameterOverride: 100,
    /**
     * Assertian that the Class is an instance of ParameterOverrideValueSet
     */
    ParameterOverrideValueSet: 101,
    /**
     * Assertian that the Class is an instance of ParameterSubscription
     */
    ParameterSubscription: 102,
    /**
     * Assertian that the Class is an instance of ParameterSubscriptionValueSet
     */
    ParameterSubscriptionValueSet: 103,
    /**
     * Assertian that the Class is an instance of ParameterType
     */
    ParameterType: 104,
    /**
     * Assertian that the Class is an instance of ParameterTypeComponent
     */
    ParameterTypeComponent: 105,
    /**
     * Assertian that the Class is an instance of ParameterValue
     */
    ParameterValue: 106,
    /**
     * Assertian that the Class is an instance of ParameterValueSet
     */
    ParameterValueSet: 107,
    /**
     * Assertian that the Class is an instance of ParameterValueSetBase
     */
    ParameterValueSetBase: 108,
    /**
     * Assertian that the Class is an instance of ParametricConstraint
     */
    ParametricConstraint: 109,
    /**
     * Assertian that the Class is an instance of Participant
     */
    Participant: 110,
    /**
     * Assertian that the Class is an instance of ParticipantPermission
     */
    ParticipantPermission: 111,
    /**
     * Assertian that the Class is an instance of ParticipantRole
     */
    ParticipantRole: 112,
    /**
     * Assertian that the Class is an instance of Person
     */
    Person: 113,
    /**
     * Assertian that the Class is an instance of PersonPermission
     */
    PersonPermission: 114,
    /**
     * Assertian that the Class is an instance of PersonRole
     */
    PersonRole: 115,
    /**
     * Assertian that the Class is an instance of Point
     */
    Point: 116,
    /**
     * Assertian that the Class is an instance of PossibleFiniteState
     */
    PossibleFiniteState: 117,
    /**
     * Assertian that the Class is an instance of PossibleFiniteStateList
     */
    PossibleFiniteStateList: 118,
    /**
     * Assertian that the Class is an instance of PrefixedUnit
     */
    PrefixedUnit: 119,
    /**
     * Assertian that the Class is an instance of Publication
     */
    Publication: 120,
    /**
     * Assertian that the Class is an instance of QuantityKind
     */
    QuantityKind: 121,
    /**
     * Assertian that the Class is an instance of QuantityKindFactor
     */
    QuantityKindFactor: 122,
    /**
     * Assertian that the Class is an instance of RatioScale
     */
    RatioScale: 123,
    /**
     * Assertian that the Class is an instance of ReferenceDataLibrary
     */
    ReferenceDataLibrary: 124,
    /**
     * Assertian that the Class is an instance of ReferencerRule
     */
    ReferencerRule: 125,
    /**
     * Assertian that the Class is an instance of ReferenceSource
     */
    ReferenceSource: 126,
    /**
     * Assertian that the Class is an instance of RelationalExpression
     */
    RelationalExpression: 127,
    /**
     * Assertian that the Class is an instance of Relationship
     */
    Relationship: 128,
    /**
     * Assertian that the Class is an instance of RelationshipParameterValue
     */
    RelationshipParameterValue: 129,
    /**
     * Assertian that the Class is an instance of RequestForDeviation
     */
    RequestForDeviation: 130,
    /**
     * Assertian that the Class is an instance of RequestForWaiver
     */
    RequestForWaiver: 131,
    /**
     * Assertian that the Class is an instance of Requirement
     */
    Requirement: 132,
    /**
     * Assertian that the Class is an instance of RequirementsContainer
     */
    RequirementsContainer: 133,
    /**
     * Assertian that the Class is an instance of RequirementsContainerParameterValue
     */
    RequirementsContainerParameterValue: 134,
    /**
     * Assertian that the Class is an instance of RequirementsGroup
     */
    RequirementsGroup: 135,
    /**
     * Assertian that the Class is an instance of RequirementsSpecification
     */
    RequirementsSpecification: 136,
    /**
     * Assertian that the Class is an instance of ReviewItemDiscrepancy
     */
    ReviewItemDiscrepancy: 137,
    /**
     * Assertian that the Class is an instance of Rule
     */
    Rule: 138,
    /**
     * Assertian that the Class is an instance of RuleVerification
     */
    RuleVerification: 139,
    /**
     * Assertian that the Class is an instance of RuleVerificationList
     */
    RuleVerificationList: 140,
    /**
     * Assertian that the Class is an instance of RuleViolation
     */
    RuleViolation: 141,
    /**
     * Assertian that the Class is an instance of ScalarParameterType
     */
    ScalarParameterType: 142,
    /**
     * Assertian that the Class is an instance of ScaleReferenceQuantityValue
     */
    ScaleReferenceQuantityValue: 143,
    /**
     * Assertian that the Class is an instance of ScaleValueDefinition
     */
    ScaleValueDefinition: 144,
    /**
     * Assertian that the Class is an instance of section
     */
    Section: 145,
    /**
     * Assertian that the Class is an instance of SharedStyle
     */
    SharedStyle: 146,
    /**
     * Assertian that the Class is an instance of SimpleParameterizableThing
     */
    SimpleParameterizableThing: 147,
    /**
     * Assertian that the Class is an instance of SimpleParameterValue
     */
    SimpleParameterValue: 148,
    /**
     * Assertian that the Class is an instance of SimpleQuantityKind
     */
    SimpleQuantityKind: 149,
    /**
     * Assertian that the Class is an instance of SimpleUnit
     */
    SimpleUnit: 150,
    /**
     * Assertian that the Class is an instance of SiteDirectory
     */
    SiteDirectory: 151,
    /**
     * Assertian that the Class is an instance of SiteDirectoryDataAnnotation
     */
    SiteDirectoryDataAnnotation: 152,
    /**
     * Assertian that the Class is an instance of SiteDirectoryDataDiscussionItem
     */
    SiteDirectoryDataDiscussionItem: 153,
    /**
     * Assertian that the Class is an instance of SiteDirectoryThingReference
     */
    SiteDirectoryThingReference: 154,
    /**
     * Assertian that the Class is an instance of SiteLogEntry
     */
    SiteLogEntry: 155,
    /**
     * Assertian that the Class is an instance of SiteReferenceDataLibrary
     */
    SiteReferenceDataLibrary: 156,
    /**
     * Assertian that the Class is an instance of Solution
     */
    Solution: 157,
    /**
     * Assertian that the Class is an instance of SpecializedQuantityKind
     */
    SpecializedQuantityKind: 158,
    /**
     * Assertian that the Class is an instance of Stakeholder
     */
    Stakeholder: 159,
    /**
     * Assertian that the Class is an instance of StakeholderValue
     */
    StakeholderValue: 160,
    /**
     * Assertian that the Class is an instance of StakeHolderValueMap
     */
    StakeHolderValueMap: 161,
    /**
     * Assertian that the Class is an instance of StakeHolderValueMapSettings
     */
    StakeHolderValueMapSettings: 162,
    /**
     * Assertian that the Class is an instance of TelephoneNumber
     */
    TelephoneNumber: 163,
    /**
     * Assertian that the Class is an instance of Term
     */
    Term: 164,
    /**
     * Assertian that the Class is an instance of TextParameterType
     */
    TextParameterType: 165,
    /**
     * Assertian that the Class is an instance of TextualNote
     */
    TextualNote: 166,
    /**
     * Assertian that the Class is an instance of Thing
     */
    Thing: 167,
    /**
     * Assertian that the Class is an instance of ThingReference
     */
    ThingReference: 168,
    /**
     * Assertian that the Class is an instance of TimeOfDayParameterType
     */
    TimeOfDayParameterType: 169,
    /**
     * Assertian that the Class is an instance of TopContainer
     */
    TopContainer: 170,
    /**
     * Assertian that the Class is an instance of UnitFactor
     */
    UnitFactor: 171,
    /**
     * Assertian that the Class is an instance of UnitPrefix
     */
    UnitPrefix: 172,
    /**
     * Assertian that the Class is an instance of UserPreference
     */
    UserPreference: 173,
    /**
     * Assertian that the Class is an instance of UserRuleVerification
     */
    UserRuleVerification: 174,
    /**
     * Assertian that the Class is an instance of ValueGroup
     */
    ValueGroup: 175
}