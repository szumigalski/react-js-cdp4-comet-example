import { Dto } from 'cdp4-sdkts/lib/dto/autogen/dto.generated'
import { UserSessionService } from 'cdp4-sdkts/lib/dal/user-session.service'
import { ThingUpdateService } from 'cdp4-sdkts/lib/dal/thing-update.service'
import { CdpServicesDal } from 'cdp4-sdkts/lib/dal/cdp-services-dal'
import { toast } from 'react-toastify'

export const initSession = async (setSession, setLoading, setDataTree) => {
    const dal = await new CdpServicesDal('1.1.0')
    const thingUpdateService = await new ThingUpdateService()
    const newSession = await new UserSessionService(dal, thingUpdateService)
    await setSession(newSession)
    await newSession.open().subscribe(
        () => {
            setLoading('Init iteration...')
        },
        (error) => {
            setLoading(null)
            toast.error('Problem with init session')
            // eslint-disable-next-line no-console
            console.log('Problem with init session: ', error, 'failed!')
        }
    )
    await thingUpdateService.thingUpdated$.subscribe(() => {
        // console.log(x._thing)
    })
}

export const getIteration = async (
    session,
    setLoading
) => {
    // siteDirectory
    const siteDirectory = await session.retrieveSiteDirectory()
    if (siteDirectory) {
        const engineeringModelSetupId = await siteDirectory.model[0]
        const engineeringModelSetup = await session.get(
            Dto.ClassKind[Dto.ClassKind.EngineeringModelSetup],
            engineeringModelSetupId,
            null
        )
        const iterationSetupId = await engineeringModelSetup.iterationSetup[0]
        const iterationSetup = await session.get(
            Dto.ClassKind[Dto.ClassKind.IterationSetup],
            iterationSetupId,
            null
        )
        const domainOfExpertiseId = await engineeringModelSetup.activeDomain[0]
        const domainOfExpertise = await session.get(domainOfExpertiseId, null)
        await session
            .readIteration(iterationSetup, domainOfExpertise)
            .subscribe(async (e) => {
                e && setLoading('ready')
                console.log('siteDirectory', siteDirectory)
            })
    } else {
        // eslint-disable-next-line no-console
        console.log('Site Directiory is empty')
    }
}