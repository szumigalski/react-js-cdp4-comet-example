import { ajax } from 'rxjs/ajax'
import { toast } from 'react-toastify'

export const loginUser = (url, login, password, setLoading, setError) => {
    console.log(url)
    setLoading('Login...')
    ajax({
        url: `${url}/login`,
        method: 'GET',
        headers: {
            'Accept-CDP': '1.1.0',
            Authorization: `Basic ${window.btoa(
                `${login}:${password}`
            )}`,
        },
    }).subscribe({
        async next() {
            toast.success('Login successfully')
            setLoading('Init session...')
        },
        error(err) {
            setLoading(false)
            if (err.status === 401) {
                toast.error('Wrong password or login')
                setError(true)
                setLoading(null)
            } else {
                toast.error('No internet connection or problem with server')
                setLoading(null)
            }
        },
    })
}