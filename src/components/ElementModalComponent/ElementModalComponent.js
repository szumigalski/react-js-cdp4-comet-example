import { Card, Modal, Typography, IconButton } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { title } from './title'
import { fields } from './elementFields'
import { renderItem } from './elementItems'
import { getData } from './getData'
import { cloneDeep } from 'lodash'
import CloseIcon from '@mui/icons-material/Close';

const ElementModal = ({ isOpen, setOpen, type, data, session, parentId}) => {
    const [modalSwitchers, setModalSwichers] = useState([])
    const [objects, setObjects] = useState(null)
    const [object, setObject] = useState(null)

    useEffect(() => {
        // Add values for modal fields
        if (data) {
            if(typeof data === 'string') {
                const newSwitchers = {}
                Object.keys(fields[type]).map((field, i) => {
                    if(field.type === 'modal') {
                        newSwitchers[i] = false
                    }
                    return 0
                })
                setModalSwichers(newSwitchers)
            } else {
                const newSwitchersList = []
                data.map((item, i) => {
                    newSwitchersList.push({})
                    Object.keys(fields[type]).map((field, j) => {
                        if(fields[type][field].type === 'modal') {
                            newSwitchersList[i][j] = false
                        }
                        return 0
                    })
                    return 0
                })
                setModalSwichers(newSwitchersList)
            }
        }
    }, [data, type])

    useEffect(() => {
        if(data) {
            if(typeof data !== 'string') {
                // Get data list from id and session
                const newData = data.map(item => getData(session, item, type, parentId))
                setObjects(newData)
            } else {
                const newData = getData(session, data, type, parentId)
                setObject(newData)
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data])

    return(<Modal
        open={isOpen}
        onClose={setOpen}
        style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
        >
            <Card style={{width: 800, display: 'flex', flexDirection: 'column', padding: 20, height: 300}}>
                <div style={{display: 'flex', justifyContent: 'space-between', marginBottom: 10}}>
                    <Typography variant="h4">{title(type)}</Typography>
                    <IconButton onClick={() => setOpen(false)}>
                        <CloseIcon />
                    </IconButton>
                </div>
                <div style={{ overflow: 'auto', height: 300}}>
                {objects && objects.map((object, i) => <Card key={i} style={{padding: 5, border: '1px solid grey'}}>{
                    Object.keys(fields[type]).map(
                        (field, j) => renderItem(
                            field,
                            fields[type][field].type,
                            objects[i][field],
                            modalSwitchers[i][j],
                            (value) => {
                                const newSwitchers = cloneDeep(modalSwitchers)
                                newSwitchers[i][j] = value
                                setModalSwichers(newSwitchers)
                            },
                            objects[i][field],
                            session,
                            type === 'iterationIid' ? object.iid : parentId
                        ))}</Card>)}
                {object &&
                    <Card style={{padding: 5, border: '1px solid grey'}}>{Object.keys(fields[type]).map(
                        (field, j) => renderItem(
                            field,
                            fields[type][field].type,
                            object[field],
                            modalSwitchers[j],
                            (value) => {
                                const newSwitchers = cloneDeep(modalSwitchers)
                                newSwitchers[j] = value
                                setModalSwichers(newSwitchers)
                            },
                            object[field],
                            session,
                            type === 'iterationIid' ? object.iid : parentId
                        ))}</Card>}
                </div>
            </Card>
    </Modal>)
}

export default ElementModal