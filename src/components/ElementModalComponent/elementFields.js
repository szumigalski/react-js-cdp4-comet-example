export const fields = {
    model: {
        name: {
            type: 'string'
        },
        shortName: {
            type: 'string'
        },
        iterationSetup: {
            type: 'modal'
        }
    },
    iterationSetup: {
        iid: {
            type: 'string'
        },
        createdOn: {
            type: 'date'
        },
        modifiedOn: {
            type: 'date'
        },
        iterationIid: {
            type: 'modal'
        },
    },
    iterationIid: {
        iid: {
            type: 'string'
        },
        createdOn: {
            type: 'date'
        },
        modifiedOn: {
            type: 'date'
        },
        element: {
            type: 'modal'
        },
        topElement: {
            type: 'modal'
        },
    },
    topElement: {
        iid: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        shortName: {
            type: 'string'
        },
        containedElement: {
            type: 'modal'
        }
    },
    element: {
        iid: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        shortName: {
            type: 'string'
        },
        parameter: {
            type: 'modal'
        }
    },
    containedElement: {
        iid: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        shortName: {
            type: 'string'
        },
        elementDefinition: {
            type: 'modal'
        },
    },
    elementDefinition: {
        iid: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        shortName: {
            type: 'string'
        },
        containedElement: {
            type: 'modal'
        },
        parameter: {
            type: 'modal'
        }
    },
    parameter: {
        iid: {
            type: 'string'
        },
        createdOn: {
            type: 'date'
        },
        modifiedOn: {
            type: 'date'
        },
        valueSet: {
            type: 'modal'
        }
    },
    valueSet: {
        iid: {
            type: 'string'
        }
    },
}