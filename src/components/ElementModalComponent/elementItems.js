import moment from 'moment'
import ElementModal from './ElementModalComponent'
import { Button } from '@mui/material'
import {labels} from './labels'

export const renderItem = (name, type, value, isOpen, setOpen, data, session, parentId) => {
    switch(type) {
        case 'string':
            return <div style={{display: 'flex', margin: 10}}>
                <b style={{width: 150}}>{labels[name] ? labels[name] : name}:</b>
                <div>{value}</div>
            </div>
        case 'date':
            return <div style={{display: 'flex', margin: 10}}>
                    <b style={{width: 150}}>{labels[name] ? labels[name] : name}:</b>
                    <div>{moment(value).format('DD-MM-YYYY, h:mm:ss a')}</div>
                </div>
        case 'modal':
            return <div style={{display: 'flex'}}>
                    <div style={{display: 'flex', margin: 10, alignItems: 'center'}}>
                        <b style={{width: 150}}>{labels[name] ? labels[name] : name}:</b>
                        <Button onClick={() => setOpen(true)} variant="outlined">Open</Button>
                    </div>
                    <ElementModal
                        isOpen={isOpen}
                        setOpen={setOpen}
                        type={name}
                        data={data}
                        session={session}
                        parentId={parentId}
                    />
                </div>
        default:
            return <div/>
    }
}