import { Dto } from 'cdp4-sdkts/lib/dto/autogen/dto.generated'

export const getData = (session, iid, type, parentId) => {
    switch(type){
        case 'model':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.EngineeringModelSetup],
                iid,
                null
            )
        case 'iterationSetup':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.IterationSetup],
                iid,
                null
            )
        case 'iterationIid':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.Iteration],
                iid,
                null
            )
        case 'topElement':
                return session.get(
                    Dto.ClassKind[Dto.ClassKind.ElementDefinition],
                    iid,
                    parentId
                )
        case 'element':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.ElementDefinition],
                iid,
                parentId
            )
        case 'containedElement':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.ElementUsage],
                iid,
                parentId
            )
        case 'elementDefinition':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.ElementDefinition],
                iid,
                parentId
            )
        case 'parameter':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.Parameter],
                iid,
                parentId
            )
        case 'valueSet':
            return session.get(
                Dto.ClassKind[Dto.ClassKind.ParameterValueSet],
                iid,
                parentId
            )
        default:
            return null
    }
}