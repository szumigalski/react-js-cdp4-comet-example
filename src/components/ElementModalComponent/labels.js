export const labels = {
    name: 'Name',
    shortName: 'Short name',
    lastModifiedOn: 'Last modified on',
    createdOn: 'Created on',
    modifiedOn: 'Modified on',
    iterationSetup: 'Iteration setup'
}