export const title = (type) => {
    switch(type) {
        case 'model':
            return 'Engineering Model Setup'
        case 'iterationSetup':
            return 'Iteration setup'
        case 'iterationIid':
            return 'Iteration'
        case 'topElement':
            return 'Element'
        case 'element':
            return 'Element'
        case 'containedElement':
            return 'Element Usage'
        case 'elementDefinition':
            return 'Element definition'
        case 'parameter':
            return 'Parameter'
        case 'valueSet':
            return 'Value set'
        default:
            break
    }
}