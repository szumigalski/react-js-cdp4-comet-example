import React, { useState } from 'react'
import { Card, TextField, Button, Typography, CircularProgress } from '@mui/material'
import AccountIcon from '@mui/icons-material/AccountCircle'
import { loginUser } from '../cdp4/login'
import pack from '../../package.json'

const LoginComponent = ({ setLoading, loading }) => {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [isError, setError] = useState(false)

    return(<div style={{width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
    {loading
        ? <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <CircularProgress />
            <div style={{marginTop: 15}}>{loading}</div>
          </div>
  : <Card style={{display: 'flex', flexDirection: 'column', width: 350, padding: 30, alignItems: 'center'}}>
      <AccountIcon style={{fontSize: 60}} color='primary' />
      <Typography>CDP4 COMET EXAMPLE</Typography>
      <TextField
        label={'server url'}
        value={pack.proxy}
        disabled
        size='small'
        style={{width: 350, marginTop: 20}}
        />
      <TextField
        label={'login'}
        value={login}
        error={isError}
        onChange={(e) => setLogin(e.target.value)}
        size='small'
        style={{width: 350, marginBottom: 20, marginTop: 20}}
        />
      <TextField
        label={'password'}
        value={password}
        error={isError}
        onChange={(e) => setPassword(e.target.value)}
        size='small'
        type='password'
        style={{width: 350, marginBottom: 20}}
        />
        <Button
            variant="outlined"
            onClick={() =>
                loginUser('http://localhost:3000', login, password, setLoading, setError)}
            >Login
        </Button>
    </Card>}
  </div>)
}

export default LoginComponent