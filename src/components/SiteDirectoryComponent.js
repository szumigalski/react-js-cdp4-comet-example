import { Card, Typography, Button } from '@mui/material'
import React, { useState , useEffect } from 'react'
import moment from 'moment'
import ElementModal from './ElementModalComponent/ElementModalComponent'

const SiteDirectoryComponent = ({ session }) => {
    const [siteDirectory, setSiteDirectory] = useState(null)
    const [isModelOpen, setModelOpen] = useState(false)

    useEffect(() => {
        session && setSiteDirectory(session.retrieveSiteDirectory())
    }, [session])

    return(<div style={{ height: '100vh', widht: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Card style={{width: '600', padding: 20}}>
                <Typography variant="h4">Site Directory</Typography>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Name:</b>
                    {siteDirectory?.name}
                </div>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Short name:</b>
                    {siteDirectory?.shortName}
                </div>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Last modified on: </b>
                    {moment(siteDirectory?.lastModifiedOn).format('DD-MM-YYYY, h:mm:ss a')}
                </div>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Modified on: </b>
                    {moment(siteDirectory?.modifiedOn).format('DD-MM-YYYY, h:mm:ss a')}
                </div>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Created on: </b>
                    {moment(siteDirectory?.createdOn).format('DD-MM-YYYY, h:mm:ss a')}
                </div>
                <div style={{margin: 10, display: 'flex'}}>
                    <b style={{width: 150}}>Model: </b>
                    <Button onClick={() => setModelOpen(true)}>Open</Button>
                </div>
            </Card>
            {siteDirectory &&<ElementModal
                type='model'
                isOpen={isModelOpen}
                setOpen={setModelOpen}
                session={session}
                data={siteDirectory?.model}
            />}
        </div>)
}

export default SiteDirectoryComponent